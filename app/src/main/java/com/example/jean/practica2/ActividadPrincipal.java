package com.example.jean.practica2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.Button;

public class ActividadPrincipal extends AppCompatActivity {

    Button botonParametro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_principal);

        botonParametro = (Button) findViewById(R.id.btnPasarParametro);


        botonParametro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActividadPrincipal.this, ActividadPasarParametro.class);
                startActivity(intent);



            }
        });
    }
}
